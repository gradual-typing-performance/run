if [[ $1 ]]; then
  RKT_VERSION="$1"
else
  RKT_VERSION="6.12"
fi
RKT_DIR="racket-${RKT_VERSION}"
RKT_TGZ="${RKT_DIR}.tgz"

wget -O ${RKT_TGZ} "https://mirror.racket-lang.org/installers/${RKT_VERSION}/racket-${RKT_VERSION}-src-builtpkgs.tgz"
tar -xzf ${RKT_TGZ}
cd ${RKT_DIR}
mkdir extra-pkgs
cd src
mkdir build
cd build
../configure && make && make install
cd ../../extra-pkgs
../bin/raco pkg install --clone gtp-util gtp-measure gtp-plot gtp-benchmarks
cd gtp-benchmarks
PLTSTDERR="error info@gtp-measure" ../../bin/raco gtp-measure -i 4 -c 10 -R 4 -S 10 --warmup 1 --bin `pwd`/../../bin/ --output ../../../data benchmarks/*/
cd ../../..
